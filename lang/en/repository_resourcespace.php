<?php
$string['pluginname'] = 'ResourceSpace';
$string['repository_resourcespace'] = 'ResourceSpace';
$string['configplugin'] = 'Configure ResourceSpace Repository';
$string['api_key'] = 'API Key';
$string['site_url'] = 'Site URL';
