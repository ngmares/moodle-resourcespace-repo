<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin is used to search and link to resources in a ResourceSpace instance
 *
 * @since 2.3
 * @package    repository_resourcespace
 * @copyright  2012 Nathan Mares
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class repository_resourcespace extends repository {
    public function __construct($repositoryid, $context = SITEID, $options = array()) {
        parent::__construct($repositoryid, $context, $options);
    }

    public function get_listing($path = '', $page = '') {
        $search_result = $this->fetch_results('', $page);
        $search_result['manage'] = get_config("resourcespace", "site_url");
        $search_result['dynload'] = true;
        $search_result['nologin'] = true;
        $search_result['nosearch'] = false;
        return $search_result;
    }

    public function fetch_results($text, $page) {
        $base_url = get_config("resourcespace", "site_url");
        $url = $base_url . "/plugins/api_search/?key=" . get_config("resourcespace", "api_key") . "&search=" . urlencode($text) . '&previewsize=col&results_per_page=20&page=' . (!empty($page) ? $page : '1');
        $results = array();

        $fetched = json_decode(file_get_contents($url));

        $mimetypes = $this->options['mimetypes'];

        foreach($fetched->resources as $result) {
            //Skip resources that are of the wrong file types. It'd be good if we could filter at the ResourceSpace end.. maybe later
            if (!($mimetypes == '*' || (is_array($mimetypes) && in_array('.'.$result->file_extension, $mimetypes)))) continue;
            $datemodified = strtotime($result->file_modified);
            $datecreated = strtotime($result->creation_date);
            $results[] = array(
                'title' => $result->field8,
                'datemodified' => $datemodified,
                'datecreated' => $datecreated,
                'thumbnail_width' => $result->thumb_width,
                'thumbnail_height' => $result->thumb_height,
                'source' => $base_url . '/pages/view.php?ref=' . $result->ref,
                'thumbnail' => $result->preview
            );
        }
        $search_result = array();
        $search_result['list'] = $results;
        $search_result['pages'] = $fetched->pagination->total_pages;
        $search_result['page'] = $fetched->pagination->page;
        return $search_result;
    }

    public function search($text, $page = 0) {
        $search_result = $this->fetch_results($text, ($page == 0 ? 1 : $page));
        return $search_result;
    }

    /**
     * Type option names
     *
     * @return array
     */
    public static function get_type_option_names() {
        return array('api_key', 'site_url');
    }

    /**
     * Type config form
     */
    public static function type_config_form($mform, $classname = 'repository') {
        $mform->addElement('text', 'api_key', get_string('api_key', 'repository_resourcespace'), array('value'=>'','size' => '40'));
        $mform->addElement('text', 'site_url', get_string('site_url', 'repository_resourcespace'), array('value'=>'','size' => '40'));
    }
    /**
     * will be called when installing a new plugin in admin panel
     *
     * @return bool
     */
    public static function plugin_init() {
        $result = true;
        // do nothing
        return $result;
    }

    /**
     * Supports file linking and copying
     *
     * @return int
     */
    public function supported_returntypes() {
        //At the moment we only support linking to ResourceSpace..
        return FILE_EXTERNAL;
    }
}
